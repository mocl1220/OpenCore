更新日志

2020-04-07编译OpenCore-0.5.8-04-07(更新版本号)

2020-04-06编译OpenCore-0.5.7官方正式版

- 将`Config-Misc-Boot-PickerAttributes`改名为`Config-Misc-Boot-ConsoleAttributes`
- 为UI配置添加`Config-Misc-Boot-PickerAttributes`

2020-04-05编译OpenCore-0.5.7-04-05

- 添加 `config-Booter-Quirks-ProtectMemoryRegions`以修复内存加载时的一些问题。
- 移除`config-Booter-Quirks-ProtectCsmRegion`用`config-Booter-Quirks-ProtectMemoryRegions`代替

2020-04-03编译OpenCore-0.5.7-04-03

- 修复了`OpenRuntime`中4K对齐，以完善在SKL上启动Linux
- 添加`config-Booter-Quirks-SyncRuntimePermissions`项，修复在CFL+上启动Linux
- 添加`config-Booter-Quirks-RebuildAppleMemoryMap`项，修复戴尔5490上启动macOS的问题
- 删除`config-Booter-Quirks-ShrinkMemoryMap`项，添加更高级的`config-Booter-Quirks-RebuildAppleMemoryMap`项
- 使用新系统(SKL+)时不用勾选`EnableWriteUnprotector`项，最好还是勾选
- 添加`Config-Misc-Debug-AppleDebug`，勾选后 boot.efi调试日志保存到OpenCore日志中，一般不勾选此参数仅适用于10.15.4及以上的版本

2020-03-30编译OpenCore-0.5.7-03-30

- 修复了10.15.4电源超时导致的内核崩溃
- 修复了OpenRuntime中4K对齐

2020-03-29编译OpenCore-0.5.7-03-29

- 优化部分代码

2020-03-24编译OpenCore-0.5.7-03-24

- 优化部分代码

2020-03-22编译OpenCore-0.5.7-03-22

- 继续更新内置固件（主要是机型）
- 优化部分代码

2020-03-20编译OpenCore-0.5.7-03-20

- 更新内置固件（主要是机型）

2020-03-18编译OpenCore-0.5.7-03-18

- 重写' readlabel '使应用程序支持' disklabel '的编码。
- 将“FwRuntimeServices”改名为“OpenRuntime”。
- 将“AppleUsbKbDxe”改名为“OpenUsbKbDxe”。
- 将“BootLiquor.efi”改名为“OpenCanopy.efi”。

2020-03-14编译OpenCore-0.5.7-03-14，

- 用“Windows”替换“BOOTCAMP Windows”。
- 在Tools中添加OpenCoreShell提供的`OpenShell.efi` 

2020-03-12编译OpenCore-0.5.7-03-12，<font color= "#FF0000" >添加ProtectUefiServices项，用于修复Z390在DevirtualiseMmio上的问题（03-12新增）</font>

2020-03-11编译OpenCore-0.5.7-03-11，优化部分代码

2020-03-08编译OpenCore-0.5.7-03-09,添加了、修复了7个项目

2020-03-08编译OpenCore-0.5.7-03-08,优化部分代码

2020-03-03编译OpenCore-0.5.7-03-03(更新版本号)

2020-03-02官方编译OpenCore-0.5.6-03-02

2020-02-24编译OpenCore-0.5.6添加开机提示音、为10.13以上的版本在boot.efi 中增加音频支持

2020-02-23编译OpenCore-0.5.6优化2个项目

2020-02-19编译OpenCore-0.5.6更新说明文件

2020-02-16-编译OpenCore-0.5.6重大调整版

2020-02-03-编译OpenCore-0.5.6

2020-01-15-编译OpenCore-0.5.5

2019-12-24-编译OpenCore-0.5.4

2019-12-19-编译OpenCore-0.5.3

特别是不能在远景论坛转载！可能被远景因此封号，谢谢合作

黑苹果OpenCore开放群，群号:9422866，注明“独行秀才Blog引入”

具体使用请参阅[OpenCore配置文字说明第三版（基于03月18日编译版）](https://shuiyunxc.gitee.io/2020/03/10/instru/index/)